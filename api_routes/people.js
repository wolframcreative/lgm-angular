// exports.list = function (req, res) {
// 	var search_terms = req.params.name,
// 		churchcode = req.session.churchcode;
// 		if( typeof(churchcode) !== "undefined"){
// 			db.People.findOne({churchcode: churchcode}, function(err, results) {
// 				if(err) {
// 					res.json({200 : "No Results!"});
// 				} else {
// 					res.json(results);
// 				}
// 			});
// 		} else {

// 		}
// };

var geocoderProvider = 'openstreetmap';
var httpAdapter = 'http';
var request = require("request"),
	geocoder = require('node-geocoder').getGeocoder(geocoderProvider, httpAdapter);



exports.list = function(req, res) {
	if(req.cookies.current_user){
		
		db.Households.find({}, function(error, response) {
			var self = this;
			if (response.length < 1){
				
				var token = req.cookies.current_user.churchcodes[0].access_token.token;
				var token_secret = req.cookies.current_user.churchcodes[0].access_token.token_secret;
				
				f1_request("people_search", {"include":"addresses,communications,attributes,requirements","status":"1"}, token, token_secret,  function(err, message, body){
					if(err) {
						res.send("413", err);
					} else {
						
						if(typeof(body.results["@count"]) !== "undefined" && body.results["@count"] > 0){
							var person = body.results.person;
							_.each(person, function(k, i){
								var household_id = k["@householdID"],
									person_id = k["@id"],
									barcode = k["barCode"];
								
									_.each(k["addresses"]["address"], function(v, i){
										var self = this;
										var geocode_string = v.address1+" "+v.city+", "+v.stProvince;
										var latLng = [];
										geocoder.geocode(geocode_string, function(errz, rez) {
											if(!errz){
											var result = rez[0];
												db.Households.insert({name:k["firstName"]+" "+k["lastName"], gender:k["gender"], maritalStatus:k["maritalStatus"], status:{name: k["status"]["name"], date: k["status"]["date"]}, household_id: household_id, barcode: barcode, person_id: person_id, addresses: k["addresses"], details:k, latLng:[result.latitude,result.longitude]}, function(err, resz){
													if(!err){
														
													}
												});
											}
									});
								});

							});
							
							res.send("200", body.results.person);
						
						} else {

							res.send("200", {"results":0});
						}
						
					}
				});
				
			} else {
				
				res.json(response);

			}
		});
	} else {
		res.send("404", {"status":"lost"});
	}


};


// http://localhost:1330/api/people/search/edna
exports.search = function(req, res) {
	if(req.cookies.current_user){
		var name = encodeURIComponent(req.params.name);
		db.Households.find({name:name}, function(error, response) {
			var self = this;
			if (response.length === 0){
				
				var token = req.cookies.current_user.churchcodes[0].access_token.token;
				var token_secret = req.cookies.current_user.churchcodes[0].access_token.token_secret;
				
				f1_request("people_search", {"searchfor": name, "include":"addresses,communications,attributes,requirements"}, token, token_secret,  function(err, message, body){
				
					if(err) {
						res.send("413", err);
					} else {
						
						if(body.results["@count"] > 0){

						var addresses = body.results.person[0].addresses;
						
						var results = body.results.person[0];
						
						// _.each(addresses.address, function(v, i){
						// 	var self = this;
						// 	var geocode_string = v.address1+" "+v.city+", "+v.stProvince;
						// 	geocoder.geocode(geocode_string, function(errz, rez) {
						// 		var result = rez[0];
	    	// 					// latLng.push(result.latitude, result.longitude);	// latLng.push()
	    	// 					v.latitude = result.latitude;
	    	// 					v.longitude = result.longitude;
						// 	});
						// 	console.log(v.latitude, v.longitude, 'loop');
						// });

						// // addresses.latLng = latLng;
						var household_id = results["@householdID"],
							person_id = results["@id"],
							barcode = results["barCode"];
						
						db.Households.insert({name:name, household_id: household_id, barcode: barcode, person_id: person_id, addresses: addresses, details: body.results.person[0]}, function(err, resz){
							if(!err){
								res.send("200", body.results.person);
							}
						});

						} else {
							res.send("200", {"results":0});
						}
						
					}
				});
				
			} else {
				
				res.json(response);

			}
		});
	} else {
		res.send("404", {"status":"lost"});
	}


};



