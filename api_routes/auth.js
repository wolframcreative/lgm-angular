
var fOne = require('oauth').OAuth,
    api_path = require('./api_path.json'),
    config = api_path.config,
    tokens = require('./tokens.json'),
    consumer_key = config.consumerKey,
    consumer_secret = config.consumerSecret,
    application_callback_path = "/authorized",
    qs = require('querystring');

exports.begin = function (req, res) {

    

     var oa = new fOne(
        "https://" +req.params.churchcode+".staging.fellowshiponeapi.com" + tokens["general"]["requestToken"],
        "https://"+req.params.churchcode+".staging.fellowshiponeapi.com"+ tokens["general"]["accessToken"],
        consumer_key,
        consumer_secret,
        "1.0",
        "http://"+ req.headers.host + application_callback_path,
        "HMAC-SHA1", 32,
        {'Accept': '*/*',
            'Connection': 'close',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit'
        }
        );

        
        oa.getOAuthRequestToken(function(error, oauth_token, oauth_token_secret, results){
            
            // If error is not false, log an error
            if (error) {
                res.send(error.data);
            } else {
                // A token has been returned. Let's store it in the session

                req.session.oa = oa;
                req.session.oauth_token = oauth_token;
                req.session.oauth_token_secret = oauth_token_secret;
                req.session.churchcode = req.params.churchcode;
                
                // Now let's redirect the user on Twitter to ask him to log in and give autorization to our application
                res.redirect("https://"+req.params.churchcode+".staging.fellowshiponeapi.com" + tokens["portalUser"]["userAuthorization"]+'?oauth_token='+oauth_token+'&oauth_callback=http://'+ req.headers.host + application_callback_path);
                

            }
        });
};

exports.finish = function (req, res, next) {
    
    if (req.session.oa) {
        
        var oa = new fOne(req.session.oa._requestUrl,
                      req.session.oa._accessUrl,
                      req.session.oa._consumerKey,
                      req.session.oa._consumerSecret,
                      req.session.oa._version,
                      req.session.oa._authorize_callback,
                      'HMAC-SHA1');
        oa.getOAuthAccessToken(req.session.oauth_token, req.session.oauth_token_secret, req.params.oauth_token,
                function(error, oauth_access_token, oauth_access_token_secret, results){
                    
                    if (error){
                        console.log("Error while getting the Access Token");
                        req.session.destroy();
                        res.redirect('/error'+error);
                    } else {
                        
                        res.cookie('oauth', {token:oauth_access_token, token_secret:oauth_access_token_secret, consumer_key: consumer_key});
                        req.session.oauth_access_token = oauth_access_token;
                        req.session.consumer_key = consumer_key;
                        
                        db.Users.find({username:req.cookies.current_user.username}, function (error, result){
                            var churchcode = {};
                            
                            churchcode.church_name = req.session.churchcode;
                            churchcode.access_token = {"token":oauth_access_token, "token_secret": oauth_access_token_secret, "consumer_key":consumer_key};
                            if(error) {
                                console.log(error);
                            } else {
                                db.Users.update({username:req.cookies.current_user.username}, {$set : {churchcodes:[churchcode]}}, function (err, resz) {
                                    if(err) {
                                        console.log(err);
                                    } else {    

                                        res.redirect('/dashboard');
                                    }
                                });
                            }
                        });
                        


                    }
                }
        );
    }
    else{
        req.session.destroy();
        res.redirect('/');
    }
};

exports.killsesh = function (req, res) {


    if(req.session.oauth || req.cookies.current_user) {
        req.session.destroy();
        res.clearCookie('current_user');
        res.json({"logged_in":false});
    
    } else {
        res.json({error: 'Couldn\'t Logout'});
    }
};


exports.login = function (req, res) {
    
    if(req.body) {

    var username = req.body.username,
        password = md5(req.body.password);
        
        
    db.Users.findOne({username:username}, function(error, response){
        
        if (!error) {
            if (response && response.length === 0) {
                res.json({error: "Username doesn\'t exist."});
            } else if (password === response.password) {
                req.cookies.current_user = response;
                res.cookie('current_user', response, {maxAge: 900000});
                res.json(response);

            }

        } else {
            res.json({
                error : "Invalid Login",
                path: "register"
            })
        }
    });
    } else {
        res.json("broken");
    }
};
exports.logged_in = function (req, res) {
    
    if(req.cookies.current_user) {
        res.json({"logged_in":true});
    } else {
        res.json({"logged_in":false});
    }
};

exports.register = function (req, res) {
    if(req.body) {
        var new_user = req.body;
        new_user.roles = ["viewer"];

        db.Users.find({username:new_user.username}, function(error, response) {
            if(response.length) {
                if(response[0].username === new_user.username) {
                    res.send({code:"User exists"});
                } 
            } else {
                new_user.password = md5(new_user.password);
                db.Users.save(new_user, function (errz, resz) {
                    if(!errz) {
                        res.send({code:"User created."});
                    } else {
                        res.send({code:"Error creating user."});
                    }
                });
            }
        });
        
    }
};

exports.resetpass = function (req, res) {

};

