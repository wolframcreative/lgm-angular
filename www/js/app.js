
var app = angular.module("app", ['ngRoute', 'leaflet-directive', 'ui.utils']);


function localConstructor() {
    this.write = function (key, obj) {
        localStorage.setItem(key, JSON.stringify(obj));
        return this;
    };
    this.get = function (key) {
        var obj = JSON.parse(localStorage.getItem(key));
        return obj;
    };
    this.clear = function () {
        localStorage.clear();
        return this;
    };
    this.remove = function (key) {
        localStorage.removeItem(key);
        return this;
    };
};

var local = new localConstructor();

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $locationProvider.html5Mode(true);

  $routeProvider.when('/', {
    templateUrl: '/src/app/views/home.ng',
    controller: 'HomeController'
  });

  $routeProvider.when('/map', {
    templateUrl: '/src/app/views/map.ng',
    controller: 'MapController'});

  $routeProvider.when('/admin', {
    templateUrl: '/src/app/views/admin.ng',
    controller: 'AdminController'});

  $routeProvider.when('/register', {
    templateUrl: '/src/app/views/register.ng',
    controller: 'RegisterController'});
  $routeProvider.when('/thanks', {
    templateUrl: '/src/app/views/thanks.ng',
    controller: 'ThanksController'
  });
  $routeProvider.when('/dashboard', {
    templateUrl: '/src/app/views/settings.ng',
    controller: 'SettingsController'});
 
  $routeProvider.otherwise({ redirectTo: '/' });
}]);
app.controller("AdminController",["$scope","$location","apiCall","AdminService", function($scope, $location, apiCall, AdminService){
	$('.angular-leaflet-map').css({'height':(window.outerHeight-100)+'px'});
	$scope.defaults = {
		center : {
			lat: 34.27201,
            lng: -118.616913,
            zoom: 14
		},
		scrollWheelZoom: false
	};
}]);

app.controller("HomeController", ['$scope', '$location','AuthenticationService','apiCall',	 function($scope, $location, AuthenticationService, apiCall) {

  
}]); 

app.controller("MapController", ['$scope', '$rootScope', '$location', 'AuthenticationService','apiCall', 'MapService', 'localIcons', function($scope, $rootScope, $location, AuthenticationService, apiCall, MapService, localIcons) {      
        'use strict';
        // $scope.details = [{'churchcode' : 'wolframcreative'}];
        angular.extend($rootScope, {
            churchDetails: [{'churchcode' : 'wolframcreative'}]
        })

        var local_icons = localIcons;

        angular.extend($scope, {
            legend: {
                position: 'bottomleft',
                colors: [ '#ff0000', '#000', '#0000ff', '#ecf386', '#ecf386', '#ecf386'],
                labels: [ 'Men', 'Women', 'Singles', 'Married','High School', 'College','General']
            },
            defaults: {
               scrollWheelZoom: false
            },
            icons: local_icons,
            mapCenter: {
                lat: 34.27201,
                lng: -118.616913,
                zoom: 12
            },
            markers: [],
            peopleList: [],
            groupsList: [],
            filterView: '/src/app/views/partials/filters.ng',
            filter: {
                people: {
                    show: true,
                    gender: 'both',
                    age: {
                        from: 0,
                        to: 120
                    }
                },
                groups: {
                    show: true,
                    gender: 'both',
                    age: 'all'
                }
            },
            updateMarkers: function () {
                $scope.markers = [];
                var peopleFilter = $scope.filter.people,
                    groupsFilter = $scope.filter.groups;
                function filterResults(filter, list) {
                    var entity, type, obj, icon, gender, age, label;
                    for (var i = 0; i < list.length; i++) {
                        entity = list[i];
                        gender = entity.gender;
                        age = entity.age;
                        if (filter.gender !== 'both' && gender !== filter.gender) continue;
                        if ( !(filter.age.from == 0 && filter.age.to == 120) && (!age || age < filter.age.from || age > filter.age.to) ) continue;
                        if (entity.hasOwnProperty('person_id'))  {
                            type = 'person';
                            gender === 'male' || !gender ? label = 'man_' : label = 'woman_';
                            icon = local_icons[label + 'icon'];
                        }
                        if (entity.hasOwnProperty('group_id')) {
                            type = 'group';
                            if (gender === 'male') {
                                icon = local_icons.men_group_icon;
                            } else if (gender === 'female') {
                                icon = local_icons.women_group_icon;
                            } else {
                                icon = local_icons.general_group_icon;
                            }
                        }
                        if (entity.addresses[0]) {
                            obj = {
                                lat: Number(entity.addresses[0].latLng[0]) || 0,
                                lng: Number(entity.addresses[0].latLng[1]) || 0,
                                message: entity.name,
                                icon: icon
                            }
                        }
                        if (obj.lat && obj.lng) {
                            $scope.markers.push(obj);
                        }
                    }
                }
                if (peopleFilter.show && $scope.peopleList.length) {
                    filterResults($scope.filter.people, $scope.peopleList);
                }
                if (groupsFilter.show && $scope.groupsList.length) {
                    filterResults($scope.filter.groups, $scope.groupsList);
                }
            },
            search: function () {
                MapService
                    .search($scope.search_field)
                        .success(function(ret) {
                            $scope.results = ret;
                        });
            },
            resetAgeRange: function (group) {
                $scope.filter[group].age.from = 0;
                $scope.filter[group].age.to = 120;
            }

             // markers: [
            //     {
            //         lat: 34.27201,
            //         lng: -118.616913,
            //         message: "Headquarters",
            //         icon: local_icons.default_icon,
            //     },
            //     {
            //         lat: 34.27401,
            //         lng: -118.616913,
            //         message: "Lifegroup Test",
            //         icon: local_icons.men_group_icon,
            //     },
            //     {
            //         lat: 34.27901,
            //         lng: -118.616913,
            //         message: "women Test",
            //         icon: local_icons.women_group_icon,
            //     }
            // ],
        });
        
    	$('.angular-leaflet-map').css({'height': (window.outerHeight-70)+'px'});
        apiCall
            .isLoggedIn()
                .then(function(response){
                    var loggedIn = response.data.logged_in;
                    if (loggedIn) {

                        //On Load

                        apiCall
                            .listPeople()
                                .success(function (result) {
                                    $scope.peopleList = result;
                                    $scope.updateMarkers();
                                });
                        
                        $scope.$watch('filter', function () {
                            $scope.updateMarkers();
                            local.write('mapFilters', $scope.filter);
                        }, true);

                        var mapFilters = local.get('mapFilters');
                        if (mapFilters) {
                            $scope.filter = mapFilters;
                        }
                        
                        $scope.ages = [];
                        for (var i = 0; i < 120 + 1; i++) {
                            $scope.ages.push(i.toString());
                        }

                    } else {
                        $location.path('/');
                    }
                });


}]); 










app.controller("NavController", ['$scope','$rootScope', '$location', 'AuthenticationService','apiCall', 'MapService', function($scope, $rootScope, $location, AuthenticationService, apiCall, MapService) {

  var scope = $rootScope;
  	  
  scope.logged_in =  apiCall.isLogged();
  	
	scope.credentials = {username: '', password: ''};

  
      
	$scope.logout = function() {
		AuthenticationService.logout();
    };

	$scope.login = function() {
  		AuthenticationService.login(scope.credentials);
	};

	scope.resetAlerts = function () {
    	$('.alert-danger').remove();
    };

    $scope.register = function () {
    	$location.path('/register');
    };
  

}]).directive('navigation', function () {
	return {
		templateUrl : '/src/app/views/nav.ng'
	}
});
app.controller("RegisterController",["$scope","$location","apiCall", function($scope, $location, apiCall){
	
	$scope.user = {username : '', password: '', email: '', churchcode:''};

	$scope.register_user = function () {
		if($scope.user.username !== "" && $scope.user.password !== "" && $scope.user.email !== '' && $scope.user.churchcode !== '') {
			apiCall.createUser($scope.user).success(function(data, status, headers, config){
                if(data.code !== "User exists") {
                    $location.path('/thanks');

                } else {
                    
                }
            });;
		} else {
			$scope.alert = true;
		}
	};
	
	$scope.resetAlerts = function (event) {
		
        $scope.alert = false;

     };
}]);
app.controller("SettingsController", ['$scope', '$location','apiCall','AuthenticationService', function($scope, $location, apiCall, AuthenticationService) {

        // $scope.churchcode = 'wolframcreative';
        $scope.f1 = {status:false, label:'Link', churchcode:''};
        apiCall.getChurchcodes().success(function(succ){
          var churchnames = []
          $.each(succ.churchcodes, function(i, v){
            churchnames.push(v.church_name);
          });

          if (churchnames.length > 0){
            $scope.f1.status = true;
          }

          $scope.f1.churchcode = churchnames;
        }).error(function(err){

        });
        $scope.register = function () {
          $location.path('/register');
        };

        $scope.auth = function () {
          if($scope.f1.churchcode !== ''){
            window.location.href= '/auth/'+ $scope.f1.churchcode;
          }
        }

      $scope.logout = function () {
            AuthenticationService.logout();
        };
}]);
app.controller("ThanksController",["$scope","$location", function($scope, $location){
	
}]);
app.factory('localIcons', ['$rootScope', function($rootScope) {
	return {
		default_icon: L.divIcon({
            iconSize: [25, 25],
            html: '<i class="boy fa fa-group fa-2x"></i><br>' + ($rootScope.churchDetails ? $rootScope.churchDetails[0].churchcode : ''),
            popupAnchor:  [0, 0]
        }),
        man_icon: L.divIcon({
            html: '<i class="man fa fa-male fa-3x"></i>',
            popupAnchor:  [05, -10] // point from which the popup should open relative to the iconAnchor
        }),
        woman_icon: L.divIcon({
            html: '<i class="woman fa fa-female fa-3x"></i>',
            popupAnchor:  [05, -10]
        }),
        girl_icon: L.divIcon({
            html: '<i class="girl fa fa-female fa-3x"></i> Teen Girl',
            popupAnchor:  [05, -10]
        }),
        boy_icon: L.divIcon({
            html: '<i class="boy fa fa-male fa-3x"></i> Teen Boy',
            popupAnchor:  [05, -10]
        }),
        baby_girl_icon: L.divIcon({
            html: '<i class="baby_g fa fa-female fa-3x"></i> Baby Girl',
            popupAnchor:  [05, -10] 
        }),
        baby_boy_icon: L.divIcon({
            html: '<i class="baby_b fa fa-male fa-3x"></i> Baby Boy',
            popupAnchor:  [05, -10]
        }),
        men_group_icon: L.divIcon({
            html: '<i class="men fa fa-group fa-3x"></i> Men\'s Group',
            popupAnchor:  [05, -10]
        }),
        women_group_icon: L.divIcon({
            html: '<i class="women fa fa-group fa-3x"></i> Women\'s Group',
            popupAnchor:  [05, -10]
        }),
        college_group_icon: L.divIcon({
            html: '<i class="college fa fa-group fa-3x"></i> College Group',
            popupAnchor:  [05, -10]
        }),
        hs_group_icon: L.divIcon({
            html: '<i class="hs fa fa-group fa-3x"></i> High School Group',
            popupAnchor:  [05, -10]
        }),
        singles_group_icon: L.divIcon({
            html: '<i class="hs fa fa-group fa-3x"></i> High School Group',
            popupAnchor:  [05, -10]
        }),
        married_group_icon: L.divIcon({
            html: '<i class="hs fa fa-group fa-3x"></i> High School Group',
            popupAnchor:  [05, -10]
        }),
        general_group_icon: L.divIcon({
            html: '<i class="hs fa fa-group fa-3x"></i> High School Group',
            popupAnchor:  [05, -10]
        })
    }
}]);
app.factory("AdminService", ['$location','$window', function($location, $window) {
  return {
    markers:function () {
      
    }
  };
}]);
app.factory('apiCall', ['$http', '$location', function($http, $location) {
   return {
        searchPeople : function(name){
            return $http.get('/api/people/'+name);
        },
        createUser : function (user) {
            return $http.post('/api/register', user);
        },
        getChurchcodes : function () {
            return $http.get('/api/churchcodes/list');
        },
        isLogged : function () {
            var logged_in = $http.get('/api/logged_in')
                .then(function(response){
                    return response.data.logged_in;
                });
            return logged_in;
        },
        isLoggedIn : function () {
            return $http.get('/api/logged_in');
        },
        listPeople : function() {
            return $http.get('/api/people/list?kc=' + new Date().getTime());
        }
   }
}]);

app.factory("AuthenticationService", ['$rootScope','$location','$window', '$http', function($rootScope, $location, $window, $http) {
  

  return {
    login: function(credentials) {

      if (credentials.username === "" && credentials.password === "") {
        $('.navbar-header').before('<div class="alert alert-danger">Something went wrong. Invalid username/password<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>');
      } else {
        $http.post('/api/login/', credentials).success(function(data, status, headers, config){
          
          if(!data.error) {

            $rootScope.logged_in = true;
            $rootScope.credentials = {username: '', password:''};
            $location.path('/map');

          } else {

              $('.navbar-header').before('<div class="alert alert-danger">Invalid Username. Try again.<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>');
            
          }
        }).error(function(data, status, headers, config){
            if(!$('.alert-danger')){
              $('.navbar-header').before('<div class="alert alert-danger">Invalid Username. Try again.<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>');
            }
        });
      }
    },
    logout: function() {

      $http.get('/api/deactivate').success(function(){
        $rootScope.logged_in = false;
        $location.path('/');


      });
    }
    
  };
}]);

app.factory("MapService", ['$location','$window', '$http', function($location, $window, $http) {
	return {
		geoLocate: function(geo) {
			return (geo) ? geo : 'need geo'; 
		},
		search : function(query) {
			return $http.get('/api/people/search/'+query);
		}
	};
}]);