app.factory("MapService", ['$location','$window', '$http', function($location, $window, $http) {
	return {
		geoLocate: function(geo) {
			return (geo) ? geo : 'need geo'; 
		},
		search : function(query) {
			return $http.get('/api/people/search/'+query);
		}
	};
}]);