app.factory('apiCall', ['$http', '$location', function($http, $location) {
   return {
        searchPeople : function(name){
            return $http.get('/api/people/'+name);
        },
        createUser : function (user) {
            return $http.post('/api/register', user);
        },
        getChurchcodes : function () {
            return $http.get('/api/churchcodes/list');
        },
        isLogged : function () {
            var logged_in = $http.get('/api/logged_in')
                .then(function(response){
                    return response.data.logged_in;
                });
            return logged_in;
        },
        isLoggedIn : function () {
            return $http.get('/api/logged_in');
        },
        listPeople : function() {
            return $http.get('/api/people/list?kc=' + new Date().getTime());
        }
   }
}]);