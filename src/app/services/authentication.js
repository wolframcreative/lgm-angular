
app.factory("AuthenticationService", ['$rootScope','$location','$window', '$http', function($rootScope, $location, $window, $http) {
  

  return {
    login: function(credentials) {

      if (credentials.username === "" && credentials.password === "") {
        $('.navbar-header').before('<div class="alert alert-danger">Something went wrong. Invalid username/password<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>');
      } else {
        $http.post('/api/login/', credentials).success(function(data, status, headers, config){
          
          if(!data.error) {

            $rootScope.logged_in = true;
            $rootScope.credentials = {username: '', password:''};
            $location.path('/map');

          } else {

              $('.navbar-header').before('<div class="alert alert-danger">Invalid Username. Try again.<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>');
            
          }
        }).error(function(data, status, headers, config){
            if(!$('.alert-danger')){
              $('.navbar-header').before('<div class="alert alert-danger">Invalid Username. Try again.<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>');
            }
        });
      }
    },
    logout: function() {

      $http.get('/api/deactivate').success(function(){
        $rootScope.logged_in = false;
        $location.path('/');


      });
    }
    
  };
}]);