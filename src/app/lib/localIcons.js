app.factory('localIcons', ['$rootScope', function($rootScope) {
	return {
		default_icon: L.divIcon({
            iconSize: [25, 25],
            html: '<i class="boy fa fa-group fa-2x"></i><br>' + ($rootScope.churchDetails ? $rootScope.churchDetails[0].churchcode : ''),
            popupAnchor:  [0, 0]
        }),
        man_icon: L.divIcon({
            html: '<i class="man fa fa-male fa-3x"></i>',
            popupAnchor:  [05, -10] // point from which the popup should open relative to the iconAnchor
        }),
        woman_icon: L.divIcon({
            html: '<i class="woman fa fa-female fa-3x"></i>',
            popupAnchor:  [05, -10]
        }),
        girl_icon: L.divIcon({
            html: '<i class="girl fa fa-female fa-3x"></i> Teen Girl',
            popupAnchor:  [05, -10]
        }),
        boy_icon: L.divIcon({
            html: '<i class="boy fa fa-male fa-3x"></i> Teen Boy',
            popupAnchor:  [05, -10]
        }),
        baby_girl_icon: L.divIcon({
            html: '<i class="baby_g fa fa-female fa-3x"></i> Baby Girl',
            popupAnchor:  [05, -10] 
        }),
        baby_boy_icon: L.divIcon({
            html: '<i class="baby_b fa fa-male fa-3x"></i> Baby Boy',
            popupAnchor:  [05, -10]
        }),
        men_group_icon: L.divIcon({
            html: '<i class="men fa fa-group fa-3x"></i> Men\'s Group',
            popupAnchor:  [05, -10]
        }),
        women_group_icon: L.divIcon({
            html: '<i class="women fa fa-group fa-3x"></i> Women\'s Group',
            popupAnchor:  [05, -10]
        }),
        college_group_icon: L.divIcon({
            html: '<i class="college fa fa-group fa-3x"></i> College Group',
            popupAnchor:  [05, -10]
        }),
        hs_group_icon: L.divIcon({
            html: '<i class="hs fa fa-group fa-3x"></i> High School Group',
            popupAnchor:  [05, -10]
        }),
        singles_group_icon: L.divIcon({
            html: '<i class="hs fa fa-group fa-3x"></i> High School Group',
            popupAnchor:  [05, -10]
        }),
        married_group_icon: L.divIcon({
            html: '<i class="hs fa fa-group fa-3x"></i> High School Group',
            popupAnchor:  [05, -10]
        }),
        general_group_icon: L.divIcon({
            html: '<i class="hs fa fa-group fa-3x"></i> High School Group',
            popupAnchor:  [05, -10]
        })
    }
}]);