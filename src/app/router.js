app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $locationProvider.html5Mode(true);

  $routeProvider.when('/', {
    templateUrl: '/src/app/views/home.ng',
    controller: 'HomeController'
  });

  $routeProvider.when('/map', {
    templateUrl: '/src/app/views/map.ng',
    controller: 'MapController'});

  $routeProvider.when('/admin', {
    templateUrl: '/src/app/views/admin.ng',
    controller: 'AdminController'});

  $routeProvider.when('/register', {
    templateUrl: '/src/app/views/register.ng',
    controller: 'RegisterController'});
  $routeProvider.when('/thanks', {
    templateUrl: '/src/app/views/thanks.ng',
    controller: 'ThanksController'
  });
  $routeProvider.when('/dashboard', {
    templateUrl: '/src/app/views/settings.ng',
    controller: 'SettingsController'});
 
  $routeProvider.otherwise({ redirectTo: '/' });
}]);