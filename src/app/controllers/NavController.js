app.controller("NavController", ['$scope','$rootScope', '$location', 'AuthenticationService','apiCall', 'MapService', function($scope, $rootScope, $location, AuthenticationService, apiCall, MapService) {

  var scope = $rootScope;
  	  
  scope.logged_in =  apiCall.isLogged();
  	
	scope.credentials = {username: '', password: ''};

  
      
	$scope.logout = function() {
		AuthenticationService.logout();
    };

	$scope.login = function() {
  		AuthenticationService.login(scope.credentials);
	};

	scope.resetAlerts = function () {
    	$('.alert-danger').remove();
    };

    $scope.register = function () {
    	$location.path('/register');
    };
  

}]).directive('navigation', function () {
	return {
		templateUrl : '/src/app/views/nav.ng'
	}
});