app.controller("MapController", ['$scope', '$rootScope', '$location', 'AuthenticationService','apiCall', 'MapService', 'localIcons', function($scope, $rootScope, $location, AuthenticationService, apiCall, MapService, localIcons) {      
        'use strict';
        // $scope.details = [{'churchcode' : 'wolframcreative'}];
        angular.extend($rootScope, {
            churchDetails: [{'churchcode' : 'wolframcreative'}]
        })

        var local_icons = localIcons;

        angular.extend($scope, {
            legend: {
                position: 'bottomleft',
                colors: [ '#ff0000', '#000', '#0000ff', '#ecf386', '#ecf386', '#ecf386'],
                labels: [ 'Men', 'Women', 'Singles', 'Married','High School', 'College','General']
            },
            defaults: {
               scrollWheelZoom: false
            },
            icons: local_icons,
            mapCenter: {
                lat: 34.27201,
                lng: -118.616913,
                zoom: 12
            },
            markers: [],
            peopleList: [],
            groupsList: [],
            filterView: '/src/app/views/partials/filters.ng',
            filter: {
                people: {
                    show: true,
                    gender: 'both',
                    age: {
                        from: 0,
                        to: 120
                    }
                },
                groups: {
                    show: true,
                    gender: 'both',
                    age: 'all'
                }
            },
            updateMarkers: function () {
                $scope.markers = [];
                var peopleFilter = $scope.filter.people,
                    groupsFilter = $scope.filter.groups;
                function filterResults(filter, list) {
                    var entity, type, obj, icon, gender, age, label;
                    for (var i = 0; i < list.length; i++) {
                        entity = list[i];
                        gender = entity.gender;
                        age = entity.age;
                        if (filter.gender !== 'both' && gender !== filter.gender) continue;
                        if ( !(filter.age.from == 0 && filter.age.to == 120) && (!age || age < filter.age.from || age > filter.age.to) ) continue;
                        if (entity.hasOwnProperty('person_id'))  {
                            type = 'person';
                            gender === 'male' || !gender ? label = 'man_' : label = 'woman_';
                            icon = local_icons[label + 'icon'];
                        }
                        if (entity.hasOwnProperty('group_id')) {
                            type = 'group';
                            if (gender === 'male') {
                                icon = local_icons.men_group_icon;
                            } else if (gender === 'female') {
                                icon = local_icons.women_group_icon;
                            } else {
                                icon = local_icons.general_group_icon;
                            }
                        }
                        if (entity.addresses[0]) {
                            obj = {
                                lat: Number(entity.addresses[0].latLng[0]) || 0,
                                lng: Number(entity.addresses[0].latLng[1]) || 0,
                                message: entity.name,
                                icon: icon
                            }
                        }
                        if (obj.lat && obj.lng) {
                            $scope.markers.push(obj);
                        }
                    }
                }
                if (peopleFilter.show && $scope.peopleList.length) {
                    filterResults($scope.filter.people, $scope.peopleList);
                }
                if (groupsFilter.show && $scope.groupsList.length) {
                    filterResults($scope.filter.groups, $scope.groupsList);
                }
            },
            search: function () {
                MapService
                    .search($scope.search_field)
                        .success(function(ret) {
                            $scope.results = ret;
                        });
            },
            resetAgeRange: function (group) {
                $scope.filter[group].age.from = 0;
                $scope.filter[group].age.to = 120;
            }

             // markers: [
            //     {
            //         lat: 34.27201,
            //         lng: -118.616913,
            //         message: "Headquarters",
            //         icon: local_icons.default_icon,
            //     },
            //     {
            //         lat: 34.27401,
            //         lng: -118.616913,
            //         message: "Lifegroup Test",
            //         icon: local_icons.men_group_icon,
            //     },
            //     {
            //         lat: 34.27901,
            //         lng: -118.616913,
            //         message: "women Test",
            //         icon: local_icons.women_group_icon,
            //     }
            // ],
        });
        
    	$('.angular-leaflet-map').css({'height': (window.outerHeight-70)+'px'});
        apiCall
            .isLoggedIn()
                .then(function(response){
                    var loggedIn = response.data.logged_in;
                    if (loggedIn) {

                        //On Load

                        apiCall
                            .listPeople()
                                .success(function (result) {
                                    $scope.peopleList = result;
                                    $scope.updateMarkers();
                                });
                        
                        $scope.$watch('filter', function () {
                            $scope.updateMarkers();
                            local.write('mapFilters', $scope.filter);
                        }, true);

                        var mapFilters = local.get('mapFilters');
                        if (mapFilters) {
                            $scope.filter = mapFilters;
                        }
                        
                        $scope.ages = [];
                        for (var i = 0; i < 120 + 1; i++) {
                            $scope.ages.push(i.toString());
                        }

                    } else {
                        $location.path('/');
                    }
                });


}]); 









