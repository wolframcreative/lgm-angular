app.controller("RegisterController",["$scope","$location","apiCall", function($scope, $location, apiCall){
	
	$scope.user = {username : '', password: '', email: '', churchcode:''};

	$scope.register_user = function () {
		if($scope.user.username !== "" && $scope.user.password !== "" && $scope.user.email !== '' && $scope.user.churchcode !== '') {
			apiCall.createUser($scope.user).success(function(data, status, headers, config){
                if(data.code !== "User exists") {
                    $location.path('/thanks');

                } else {
                    
                }
            });;
		} else {
			$scope.alert = true;
		}
	};
	
	$scope.resetAlerts = function (event) {
		
        $scope.alert = false;

     };
}]);