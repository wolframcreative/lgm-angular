app.controller("SettingsController", ['$scope', '$location','apiCall','AuthenticationService', function($scope, $location, apiCall, AuthenticationService) {

        // $scope.churchcode = 'wolframcreative';
        $scope.f1 = {status:false, label:'Link', churchcode:''};
        apiCall.getChurchcodes().success(function(succ){
          var churchnames = []
          $.each(succ.churchcodes, function(i, v){
            churchnames.push(v.church_name);
          });

          if (churchnames.length > 0){
            $scope.f1.status = true;
          }

          $scope.f1.churchcode = churchnames;
        }).error(function(err){

        });
        $scope.register = function () {
          $location.path('/register');
        };

        $scope.auth = function () {
          if($scope.f1.churchcode !== ''){
            window.location.href= '/auth/'+ $scope.f1.churchcode;
          }
        }

      $scope.logout = function () {
            AuthenticationService.logout();
        };
}]);