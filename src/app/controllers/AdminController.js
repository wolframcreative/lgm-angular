app.controller("AdminController",["$scope","$location","apiCall","AdminService", function($scope, $location, apiCall, AdminService){
	$('.angular-leaflet-map').css({'height':(window.outerHeight-100)+'px'});
	$scope.defaults = {
		center : {
			lat: 34.27201,
            lng: -118.616913,
            zoom: 14
		},
		scrollWheelZoom: false
	};
}]);