var express = require('express'),
    http = require('http'),
    RedisStore = require('connect-redis')(express);
    api_path = require('./api_routes/api_path.json');
    config = api_path.config,
    tokens = require('./api_routes/tokens.json'),
    auth = require('./api_routes/auth'),
    user = require('./api_routes/user'),
    people = require('./api_routes/people'),
    churchcodes = require('./api_routes/churchcodes'),
    request = require('request');

    _ = require("underscore");
    databaseUrl = "lgmapper:lgmapper@paulo.mongohq.com:10053/lg-map"; // "username:password@example.com/mydb"
    // databaseUrl = "lgm:lgm@localhost/lgm",
    collections = ["People", "Groups", "Churches", "Users", "Households"];
    db = require("mongojs").connect(databaseUrl, collections);
    md5 = require("MD5");

    f1 = function (route) {
        return {path: config.url+api_path[route].path+'.json', method:api_path[route].verb};
    };

    f1_request = function (path, params, token, token_secret, callback) {
    var url = f1(path);
    
    request({
        url: url.path,
        method: url.method,
        qs: params || {},
        oauth: {
            consumer_key: config.consumerKey,
            consumer_secret: config.consumerSecret,
            token: token,
            token_secret: token_secret
        },
        json: true
    }, callback);
    }

var app = express();

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, api_token, user_token');
    if ('OPTIONS' == req.method) {
        res.send(200);
    } else {
        next();
    }
};

app.configure(function(){
    app.set('port', process.env.PORT || 3000);
    // app.use(allowCrossDomain);
    app.use('/www', express.static(__dirname + '/www'));
    app.use('/src', express.static(__dirname + '/src'));
    app.use('/vendor', express.static(__dirname + '/vendor'));
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.methodOverride());
    app.use(express.session({store: new RedisStore({
    host: 'pub-redis-14861.us-east-1-3.2.ec2.garantiadata.com',
    port: 14861,
    pass: 'sermonN0tes'
  }),
  secret: '1234567890QWERTY'}));
    app.set('views', __dirname + '/www');
    app.engine('html', require('ejs').renderFile);
});

app.configure('development', function(){
    app.use(express.errorHandler());
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.use(express.logger());
    app.use(express.cookieParser());
    app.use(express.session({secret: 'God is Good.'}));
});

// API routing starts here
app.get('/error', function(res, req){
    res.send('There was an error');
});
app.post('/api/login/', auth.login);
app.post('/api/register', auth.register);
app.get('/api/logged_in', auth.logged_in);
app.get('/auth/:churchcode', auth.begin);
app.get('/authorized', auth.finish);
app.get('/api/deactivate', auth.killsesh);
app.get('/api/resetpass', auth.resetpass);

app.get('/api/get_user', user.get_user);
app.post('/api/save_user', user.save_user);

app.get('/api/people/search/:name', people.search);
app.get('/api/people/list', people.list);


app.get('/api/churchcodes/list', churchcodes.churchcodeList);


// Handle other routing.
app.get('/*', function (req, res) {
    res.render('index.html');
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});